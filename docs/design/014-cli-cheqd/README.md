# Cheqd-ledger Interface

This design proposes the list of commands to Indy CLI to handle export/import wallet operations.

## Goals and ideas
Cheqd-ledger CLI should provide ability to perform following operation:
* Allow operations with accounts: get info, add new account
* Allow operations with balances: get balance, send coins to other account
* Allow work with nyms

## New CLI commands

### Get account from ledger

Add info account from ledger.

```cheqd-cli
cheqd> cheqd-ledger get-account address=<account-address>
```

Returns:

* info account or error message

Example:

```cheqd-cli
cheqd> cheqd-ledger get-account address=cosmos1fknpjldck6n3v2wu86arpz8xjnfc60f99ylcjd
```

Example returns:
```
{
    "account": {
        "base_account": {
            "address":"cosmos1fknpjldck6n3v2wu86arpz8xjnfc60f99ylcjd",
            "pub_key":null,
            "account_number":1,
            "sequence":0
        }
    }
}
```

### Create NYM

Create new NYM.

```cheqd-cli
cheqd> cheqd-ledger create-nym did=<did> verkey=<verkey> key_alias=<key-alias> max_coin=<max-coins-for-tx> max_gas=<max-fees-for-tx> denom=<denomination> role=<role-user> memo=<comment-for-tx>
```

Returns:

* NYM id or error message

Example:

```cheqd-cli
cheqd> cheqd-ledger create-nym did=my_did verkey=my_verkey key_alias=my_key max_coin=500 max_gas=10000000 denom=cheq role=role memo=memo
```

Example returns:
```
{"id":0}
```

### Get NYM

Get info NYM

```cheqd-cli
cheqd> cheqd-ledger get-nym id=<nym-id>
```

Returns:

* NYM info or error message

Example:

```cheqd-cli
cheqd> cheqd-ledger get-nym id=1
```

Example returns:
```
{
    "nym": {
        "creator":"cosmos1fknpjldck6n3v2wu86arpz8xjnfc60f99ylcjd",
        "id":1,
        "alias":"my_pool",
        "verkey":"my_verkey",
        "did":"my_did",
        "role":"role"
    }
}
```

### Bank send

Send some coins between accounts.

```cheqd-cli
cheqd> cheqd-ledger bank-send from=<sender-address> to=<getter-address> amount=<amount-coins> denom=<denomination> key_alias=<key-alias> max_coin=<max-coins-for-tx> max_gas=<max-fees-for-tx>
```

Returns:

* Sucess or error message

Example:

```cheqd-cli
cheqd> cheqd-ledger bank-send from=cosmos1fknpjldck6n3v2wu86arpz8xjnfc60f99ylcjd to=cosmos1cyzz4pg6snd6ul9ckh9stq87lfxglz47tdwwnx amount=100 denom=cheq key_alias=qwerty max_coin=100 max_gas=10000000
```

Example returns:
```
{}
```

