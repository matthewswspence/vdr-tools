#!/bin/bash

if [ $# -ne 1 ]
  then
    echo "ERROR: Incorrect number of arguments"
    echo "Usage:"
    echo "$0 <debug|release>"
    exit 1
fi

BUILD_TYPE=$1

if [ $BUILD_TYPE == 'release' ]
  then
    CARGO_FLAGS='--release'
  else
    CARGO_FLAGS=''
fi

set -eux

cp libvdrtools/target/${BUILD_TYPE}/libvdrtools.so cli
pushd cli
# Build cli without cheqd feature first just for check
LIBRARY_PATH=./ RUST_BACKTRACE=1 cargo build ${CARGO_FLAGS} --features "fatal_warnings"
LIBRARY_PATH=./ RUST_BACKTRACE=1 cargo build ${CARGO_FLAGS} --features "fatal_warnings cheqd"
popd
