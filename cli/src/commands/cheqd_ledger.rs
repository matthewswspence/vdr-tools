extern crate regex;
extern crate chrono;

use crate::command_executor::{Command, CommandContext, CommandMetadata, CommandParams, CommandGroup, CommandGroupMetadata};
use crate::commands::*;

use crate::libvdrtools::cheqd_ledger::CheqdLedger;
use crate::libvdrtools::cheqd_pool::CheqdPool;
use crate::libvdrtools::cheqd_keys::CheqdKeys;
use crate::libvdrtools::did::Did;

use serde_json::{Value};

const RESPONSE: &str = "response";
const LAST_BLOCK_HEIGHT: &str = "last_block_height";
pub const SIMULATE_REQUEST: bool = false;

pub mod group {
    use super::*;

    command_group!(CommandGroupMetadata::new("cheqd-ledger", "Cheqd ledger management commands"));
}

pub mod get_account_command {
    use super::*;

    command!(CommandMetadata::build("get-account", "Query cheqd account.")
                .add_required_param("address", "Address of account")
                .add_example("cheqd-ledger get-account address=cheqd1mhl8w0xvdl3r6xf67utnqna77q0vjqgzenk7yv")
                .finalize()
    );

    fn execute(ctx: &CommandContext, params: &CommandParams) -> Result<(), ()> {
        trace!("execute >> ctx {:?} params {:?}", ctx, params);
        let address = get_str_param("address", params).map_err(error_err!())?;
        let pool_alias = ensure_cheqd_connected_pool(ctx)?;

        let query = CheqdLedger::build_query_account(address)
            .map_err(|err| handle_indy_error(err, None, None, None))?;
        let response = CheqdPool::abci_query(&pool_alias, &query)
            .map_err(|err| handle_indy_error(err, None, None, None))?;
        let parsed_response = CheqdLedger::parse_query_account_resp(&response)
            .map_err(|err| handle_indy_error(err, None, None, None))?;

        println!("Account info: {}",parsed_response);
        trace!("execute << {:?}", parsed_response);

        Ok(())
    }
}

pub mod create_did_command {
    use super::*;

    command!(CommandMetadata::build("create-did", "Create did.")
                .add_required_param("did", "DID of identity presented in Ledger")
                .add_required_param("key_alias", "Alias of key")
                .add_required_param("max_coin", "Max amount coins for transaction")
                .add_required_param("max_gas", "Max amount gas for transaction")
                .add_required_param("denom", "Denom is currency for transaction")
                .add_optional_param("memo", "Memo is optional param. It has any arbitrary memo to be added to the transaction")
                .add_optional_param("simulate", "Simulate is optional param. Simulate the request (False by default). If true then created request will be simulated and the result of simulation will be printed.")
                .add_example("cheqd-ledger create-did did=my_did max_coin=500 max_gas=10000000 denom=cheq memo=memo")
                .add_example("cheqd-ledger create-did did=my_did max_coin=500 max_gas=10000000 denom=cheq memo=memo simulate=true")
                .finalize()
    );

    fn execute(ctx: &CommandContext, params: &CommandParams) -> Result<(), ()> {
        trace!("execute >> ctx {:?} params {:?}", ctx, params);
        // Parse input params
        let did = get_str_param("did", params).map_err(error_err!())?;
        let key_alias = get_str_param("key_alias", params).map_err(error_err!())?;
        let max_coin = get_str_param("max_coin", params).map_err(error_err!())?
            .parse::<u64>().map_err(|_| println_err!("Invalid format of input data: max_coin must be integer"))?;
        let max_gas = get_str_param("max_gas", params).map_err(error_err!())?
            .parse::<u64>().map_err(|_| println_err!("Invalid format of input data: max_gas must be integer"))?;
        let denom = get_str_param("denom", params).map_err(error_err!())?;
        let memo = get_opt_str_param("memo", params).map_err(error_err!())?.unwrap_or("");
        let simulate = get_opt_bool_param("simulate", params).map_err(error_err!())?.unwrap_or(SIMULATE_REQUEST);

        // Get additional data
        let pool_alias = ensure_cheqd_connected_pool(ctx)?;
        let wallet_handle = ensure_opened_wallet_handle(&ctx)?;
        let timeout_height = get_timeout_height(&pool_alias)?;
        let key_info = CheqdKeys::get_info(wallet_handle, key_alias)
            .map_err(|err| handle_indy_error(err, None, None, None))?;

        // ToDo: create a lib function for making short DID from fully
        // Get full verkey cause user uses abbreviated
        let ledger_verkey = get_full_verkey(wallet_handle, did)?;

        // Get Public key for making Cosmos signature
        let key_info_json: Value = serde_json::from_str(&key_info)
            .map_err(|err| println_err!("Invalid data has been received: {:?}", err))?;
        let account_id = key_info_json["account_id"].as_str().unwrap();
        let pubkey = key_info_json["pub_key"].as_str().unwrap();

        // Create MsgCreateDid write request
        let request_bytes = CheqdLedger::build_msg_create_did(&did, &ledger_verkey)
            .map_err(|err| handle_indy_error(err, None, None, None))?;

        // Create indy signature and insert it into MsgWriteRequest
        let request_signed = CheqdLedger::sign_msg_write_request(wallet_handle, did, &request_bytes)
            .map_err(|err| handle_indy_error(err, None, None, None))?;

        // Get the last account_sequence - count of request which were signed by this account
        let (account_number, account_sequence) = get_base_account_number_and_sequence(account_id, &pool_alias)?;

        // Create Cosmos Tx structure
        let tx = CheqdLedger::build_tx(
            &pool_alias,
            pubkey,
            &request_signed,
            account_number,
            account_sequence,
            max_gas,
            max_coin,
            denom,
            timeout_height,
            memo
        ).map_err(|err| handle_indy_error(err, None, None, None))?;

        // Sign Tx object by using Cosmos signature
        let signed_tx = CheqdLedger::sign_tx(wallet_handle, key_alias, &tx)
            .map_err(|err| handle_indy_error(err, None, None, None))?;

        // Send and get response from the ledger
        let parsed_response = if !simulate {
            let response = CheqdPool::broadcast_tx_commit(&pool_alias, &signed_tx)
                .map_err(|err| handle_indy_error(err, None, None, None))?;
            let parsed_response = CheqdLedger::parse_msg_create_did_resp(&response)
                .map_err(|err| handle_indy_error(err, None, None, None))?;

            parsed_response
        } else {
            simulate_tx(&pool_alias, &signed_tx)?
        };

        println_succ!("Response from ledger: {:?}", parsed_response);
        trace!("execute << {:?}", parsed_response);
        Ok(())
    }
}

pub mod update_did_command {
    use super::*;

    command!(CommandMetadata::build("update-did", "Create did.")
                .add_required_param("did", "DID of identity presented in Ledger")
                .add_required_param("version_id", "Transaction hash of previously added DID (NYM)")
                .add_required_param("key_alias", "Alias of key")
                .add_required_param("max_coin", "Max amount coins for transaction")
                .add_required_param("max_gas", "Max amount gas for transaction")
                .add_required_param("denom", "Denom is currency for transaction")
                .add_required_param("memo", "Memo is optional param. It has any arbitrary memo to be added to the transaction")
                .add_optional_param("simulate", "Simulate is optional param. Simulate the request (False by default). If true then created request will be simulated and the result of simulation will be printed.")
                .add_example("cheqd-ledger update-did did=my_did verkey=my_verkey version_id=1 max_coin=500 max_gas=10000000 denom=cheq memo=memo")
                .add_example("cheqd-ledger update-did did=my_did verkey=my_verkey version_id=1 max_coin=500 max_gas=10000000 denom=cheq memo=memo simulate=true")
                .finalize()
    );

    fn execute(ctx: &CommandContext, params: &CommandParams) -> Result<(), ()> {
        trace!("execute >> ctx {:?} params {:?}", ctx, params);
        // Parse input params
        let did = get_str_param("did", params).map_err(error_err!())?;
        let version_id = get_str_param("version_id", params).map_err(error_err!())?;
        let key_alias = get_str_param("key_alias", params).map_err(error_err!())?;
        let max_coin = get_str_param("max_coin", params).map_err(error_err!())?
            .parse::<u64>().map_err(|_| println_err!("Invalid format of input data: max_coin must be integer"))?;
        let max_gas = get_str_param("max_gas", params).map_err(error_err!())?
            .parse::<u64>().map_err(|_| println_err!("Invalid format of input data: max_gas must be integer"))?;
        let denom = get_str_param("denom", params).map_err(error_err!())?;
        let memo = get_opt_str_param("memo", params).map_err(error_err!())?.unwrap_or("");
        let simulate = get_opt_bool_param("simulate", params).map_err(error_err!())?.unwrap_or(SIMULATE_REQUEST);

        // Get additional data
        let pool_alias = ensure_cheqd_connected_pool(ctx)?;
        let wallet_handle = ensure_opened_wallet_handle(&ctx)?;
        let timeout_height = get_timeout_height(&pool_alias)?;
        let key_info = CheqdKeys::get_info(wallet_handle, key_alias)
            .map_err(|err| handle_indy_error(err, None, None, None))?;

        // ToDo: create a lib function for making short DID from fully
        // Get full verkey cause user use abbreviated
        let ledger_verkey = get_full_verkey(wallet_handle, did)?;

        // Get Public key for making Cosmos signature
        let key_info_json: Value = serde_json::from_str(&key_info)
            .map_err(|err| println_err!("Invalid data has been received: {:?}", err))?;
        let account_id = key_info_json["account_id"].as_str().unwrap();
        let pubkey = key_info_json["pub_key"].as_str().unwrap();

        // Build MsgUpdateDid write request
        let request_bytes = CheqdLedger::build_msg_update_did(&did, &ledger_verkey, version_id)
            .map_err(|err| handle_indy_error(err, None, None, None))?;

        // Create indy signature and insert it into MsgWriteRequest
        let request_signed = CheqdLedger::sign_msg_write_request(wallet_handle, did, &request_bytes)
            .map_err(|err| handle_indy_error(err, None, None, None))?;

        // Get the last account_sequence - count of request which were signed by this account
        let (account_number, account_sequence) = get_base_account_number_and_sequence(account_id, &pool_alias)?;

        // Create Cosmos Tx structure
        let tx = CheqdLedger::build_tx(
            &pool_alias,
            pubkey,
            &request_signed,
            account_number,
            account_sequence,
            max_gas,
            max_coin,
            denom,
            timeout_height,
            memo
        ).map_err(|err| handle_indy_error(err, None, None, None))?;

        // Sign Tx object by using Cosmos signature
        let signed_tx =  CheqdLedger::sign_tx(wallet_handle, key_alias, &tx)
            .map_err(|err| handle_indy_error(err, None, None, None))?;

        // Send and get response from the ledger
        let parsed_response = if !simulate {
            let response = CheqdPool::broadcast_tx_commit(&pool_alias, &signed_tx)
                .map_err(|err| handle_indy_error(err, None, None, None))?;
            let parsed_response = CheqdLedger::parse_msg_update_did_resp(&response)
                .map_err(|err| handle_indy_error(err, None, None, None))?;

            parsed_response
        } else {
            simulate_tx(&pool_alias, &signed_tx)?
        };

        println_succ!("Response from ledger: {:?}", parsed_response);
        trace!("execute << {:?}", parsed_response);
        Ok(())
    }
}


pub mod get_did_command {
    use super::*;

    command!(CommandMetadata::build("get-did", "Get did from Ledger.")
                .add_required_param("did", "Unique identifier for DID")
                .add_example("cheqd-ledger get-did did=0")
                .finalize()
    );

    fn execute(ctx: &CommandContext, params: &CommandParams) -> Result<(), ()> {
        trace!("execute >> ctx {:?} params {:?}", ctx, params);
        // Parse input params
        let did = get_str_param("did", params).map_err(error_err!())?;
        let pool_alias = ensure_cheqd_connected_pool(ctx)?;

        // Build GetDid request.
        let query = CheqdLedger::build_query_get_did(did)
            .map_err(|err| handle_indy_error(err, None,
                                             Some(pool_alias.as_str()), None))?;
        let response = CheqdPool::abci_query(&pool_alias, &query)
            .map_err(|err| handle_indy_error(err, None,
                                             Some(pool_alias.as_str()), None))?;
        let parsed_response = CheqdLedger::parse_query_get_did_resp(&response)
            .map_err(|err| handle_indy_error(err, None,
                                             Some(pool_alias.as_str()), None))?;

        println_succ!("DID info: {}", parsed_response);
        trace!("execute << {:?}", parsed_response);

        Ok(())
    }
}

pub mod bank_send_command {
    use super::*;

    command!(CommandMetadata::build("bank-send", "Send coins between accounts.")
                .add_required_param("from", "Address for sending coins")
                .add_required_param("to", "Address for getting coins")
                .add_required_param("amount", "Amount coins for send transaction")
                .add_required_param("denom", "Denom of coins")
                .add_required_param("key_alias", "Key alias")
                .add_required_param("max_coin", "Max amount coins for transaction")
                .add_required_param("max_gas", "Max amount gas for transaction")
                .add_optional_param("memo", "Memo is optional param. It has any arbitrary memo to be added to the transaction")
                .add_optional_param("simulate", "Simulate is optional param. Simulate the request (False by default). If true then created request will be simulated and the result of simulation will be printed.")
                .add_example("cheqd-ledger bank-send from=sender_address to=getter_address amount=100 denom=cheq key_alias=my_key max_coin=100 max_gas=10000000")
                .add_example("cheqd-ledger bank-send from=sender_address to=getter_address amount=100 denom=cheq key_alias=my_key max_coin=100 max_gas=10000000 simulate=true")

                .finalize()
    );

    fn execute(ctx: &CommandContext, params: &CommandParams) -> Result<(), ()> {
        trace!("execute >> ctx {:?} params {:?}", ctx, params);
        let from = get_str_param("from", params).map_err(error_err!())?;
        let to = get_str_param("to", params).map_err(error_err!())?;
        let amount = get_str_param("amount", params).map_err(error_err!())?;
        let denom = get_str_param("denom", params).map_err(error_err!())?;
        let key_alias = get_str_param("key_alias", params).map_err(error_err!())?;
        let max_coin = get_str_param("max_coin", params).map_err(error_err!())?
            .parse::<u64>().map_err(|_| println_err!("Invalid format of input data: max_coin must be integer"))?;
        let max_gas = get_str_param("max_gas", params).map_err(error_err!())?
            .parse::<u64>().map_err(|_| println_err!("Invalid format of input data: max_gas must be integer"))?;
        let memo = get_opt_str_param("memo", params).map_err(error_err!())?.unwrap_or("");
        let simulate = get_opt_bool_param("simulate", params).map_err(error_err!())?.unwrap_or(SIMULATE_REQUEST);

        let pool_alias = ensure_cheqd_connected_pool(ctx)?;

        let request = CheqdLedger::build_msg_send(from, to, amount, denom)
            .map_err(|err| handle_indy_error(err, None, None, None))?;
        let signed_tx = build_and_sign_tx(ctx, &pool_alias, &request, key_alias, denom, max_gas, max_coin, memo)?;

        let parsed_response = if !simulate {
            let response = CheqdPool::broadcast_tx_commit(&pool_alias, &signed_tx)
                .map_err(|err| handle_indy_error(err, None, None, None))?;

            CheqdLedger::parse_msg_send_resp(&response)
                .map_err(|err| handle_indy_error(err, None, None, None))?;
        } else {
            simulate_tx(&pool_alias, &signed_tx)?;
        };

        trace!("execute << {:?}", parsed_response);
        Ok(())
    }
}


pub mod get_balance_command {
    use super::*;

    command!(CommandMetadata::build("get-balance", "Get balance from Ledger.")
                .add_required_param("address", "Account identifier.")
                .add_required_param("denom", "Account balance denom")
                .add_example("cheqd-ledger get-balance address=cosmos1mhl8w0xvdl3r6xf67utnqna77q0vjqgzenk7yv")
                .finalize()
    );

    fn execute(ctx: &CommandContext, params: &CommandParams) -> Result<(), ()> {
        trace!("execute >> ctx {:?} params {:?}", ctx, params);
        let address = get_str_param("address", params).map_err(error_err!())?;
        let denom = get_str_param("denom", params).map_err(error_err!())?;
        let pool_alias = ensure_cheqd_connected_pool(ctx)?;

        let query = CheqdLedger::build_query_balance(address, denom)
            .map_err(|err| handle_indy_error(err, None, None, None))?;
        let response = CheqdPool::abci_query(&pool_alias, &query)
            .map_err(|err| handle_indy_error(err, None, None, None))?;
        let parsed_response = CheqdLedger::parse_query_balance_resp(&response)
            .map_err(|err| handle_indy_error(err, None, None, None))?;

        println_succ!("Balance info: {}",parsed_response);
        trace!("execute << {:?}", parsed_response);

        Ok(())
    }
}

fn build_and_sign_tx(ctx: &CommandContext,
                        pool_alias: &str,
                        request: &[u8],
                        key_alias: &str,
                        denom: &str,
                        max_gas: u64,
                        max_coin: u64,
                        memo: &str) -> Result<Vec<u8>, ()> {
    let wallet_handle = ensure_opened_wallet_handle(&ctx)?;
    let timeout_height = get_timeout_height(pool_alias)?;

    let key_info = CheqdKeys::get_info(wallet_handle, key_alias)
        .map_err(|err| handle_indy_error(err, None, None, None))?;
    let key_info_json: Value = serde_json::from_str(&key_info)
        .map_err(|err| println_err!("Invalid data has been received: {:?}", err))?;
    let pubkey = key_info_json["pub_key"].as_str().unwrap();

    let account_id = key_info_json["account_id"].as_str().unwrap();
    let (account_number, account_sequence) = get_base_account_number_and_sequence(account_id, pool_alias)?;

    let tx = CheqdLedger::build_tx(
        &pool_alias,
        pubkey,
        &request,
        account_number,
        account_sequence,
        max_gas,
        max_coin,
        denom,
        timeout_height,
        memo
    ).map_err(|err| handle_indy_error(err, None, None, None))?;

    let signed  =  CheqdLedger::sign_tx(wallet_handle, key_alias, &tx)
        .map_err(|err| handle_indy_error(err, None, None, None))?;

    Ok(signed)
}

fn simulate_tx(pool_alias: &str, signed_tx: &[u8]) -> Result<String, ()> {
    let query = CheqdLedger::build_query_simulate(&signed_tx)
        .map_err(|err| handle_indy_error(err, None, None, None))?;
    let response = CheqdPool::abci_query(&pool_alias, &query)
        .map_err(|err| handle_indy_error(err, None,
                                        Some(pool_alias), None))?;
    let parsed_response = CheqdLedger::parse_query_simulate_resp(&response)
        .map_err(|err| handle_indy_error(err, None, None, None))?;

    println!("Transaction was not sent to the Ledger.");

    let simulate_response_json: Value = serde_json::from_str(&parsed_response)
        .map_err(|err| println_err!("Invalid data has been received: {:?}", err))?;

    let gas_used = simulate_response_json.as_object().unwrap()
        .get("gas_info").unwrap().as_object().unwrap()
        .get("gas_used").unwrap().as_u64().unwrap();

    println!("The amount of gas used to broadcast the transaction: {}", gas_used);

    Ok(parsed_response)
}

fn get_base_account(account: &serde_json::map::Map<String, Value>) -> Option<&serde_json::Map<String,Value>> {
    if account["type_url"] == "BaseAccount" {
        return Some(account["value"].as_object().unwrap())
    } else if account["type_url"] == "ModuleAccount" {
        let module_account = account["value"].as_object().unwrap();
        return Some(module_account["base_account"].as_object().unwrap())
    } else if account["type_url"] == "BaseVestingAccount" {
        let base_vesting_account = account["value"].as_object().unwrap();
        return Some(base_vesting_account["base_account"].as_object().unwrap())
    } else if account["type_url"] == "ContinuousVestingAccount" {
        let continuous_vesting_account = account["value"].as_object().unwrap();
        let base_vesting_account = continuous_vesting_account["value"].as_object().unwrap();
        return Some(base_vesting_account["base_account"].as_object().unwrap())
    } else if account["type_url"] == "DelayedVestingAccount" {
        let delayed_vesting_account = account["delayed_vesting_account"].as_object().unwrap();
        let base_vesting_account = delayed_vesting_account["value"].as_object().unwrap();
        return Some(base_vesting_account["base_account"].as_object().unwrap())
    } else if account["type_url"] == "PeriodicVestingAccount" {
        let periodic_vesting_account = account["periodic_vesting_account"].as_object().unwrap();
        let base_vesting_account = periodic_vesting_account["value"].as_object().unwrap();
        return Some(base_vesting_account["base_account"].as_object().unwrap())
    }
    None
}

fn get_base_account_number_and_sequence(address: &str, pool_alias: &str) -> Result<(u64, u64), ()> {
    let query = CheqdLedger::build_query_account(address)
        .map_err(|err| handle_indy_error(err, None, None, None))?;

    let response = CheqdPool::abci_query(pool_alias, &query)
        .map_err(|err| handle_indy_error(err, None, None, None))?;

    let parsed_response = CheqdLedger::parse_query_account_resp(&response)
        .map_err(|err| handle_indy_error(err, None, None, None))?;

    let parsed_response: Value = match serde_json::from_str(&parsed_response) {
        Ok(json) => json,
        Err(_) => {
            println_err!("Invalid json response. Can't parse response.");
            return Err(())
        }
    };

    if parsed_response["account"].is_null() {
        println_err!("Invalid json response. Can't get account from response.");
        return Err(());
    }
    let account = parsed_response["account"].as_object().unwrap();
    let base_account = match get_base_account(account) {
        Some(ac) =>  ac,
        None => {
            println_err!("Invalid account. Can't get base account from account.");
            return Err(());
        }
    };

    if !base_account.contains_key("account_number") {
        println_err!("Invalid base account. Can't get account number from base account.");
        return Err(());
    }
    let account_number = base_account["account_number"].as_u64().unwrap();

    if !base_account.contains_key("sequence") {
        println_err!("Invalid base account. Can't get sequence from base account.");
        return Err(());
    }
    let account_sequence = base_account["sequence"].as_u64().unwrap();

    Ok((account_number, account_sequence))
}

pub fn get_timeout_height(pool_alias: &str) -> Result<u64, ()> {
    let info = match CheqdPool::abci_info(&pool_alias) {
        Ok(resp) => {
            println_succ!("Abci-info request result \"{}\"", resp);
            Ok(resp)
        },
        Err(err) => {
            handle_indy_error(err, None, Some(&pool_alias), None);
            Err(())
        },
    };
    let info: Value = serde_json::from_str(&info?)
        .map_err(|_| println_err!("Wrong data of Abci-info has been received"))?;

    let current_height = info[RESPONSE][LAST_BLOCK_HEIGHT].as_str().unwrap_or_default()
        .parse::<u64>().map_err(|_| println_err!("Invalid getting abci-info response. Height must be integer."))?;

    const TIMEOUT: u64 = 20;

    return Ok(current_height + TIMEOUT);
}

pub fn get_full_verkey(wallet_handle: WalletHandle, did: &str) -> Result<String,()> {
    let ledger_did = Did::get_did_with_meta(wallet_handle, did).unwrap();
    let ledger_did:serde_json::Value = serde_json::from_str(&ledger_did).unwrap();
    let ledger_verkey = ledger_did["verkey"].as_str().unwrap().to_string();
    Ok(ledger_verkey)
}

#[cfg(test)]
pub mod tests {
    use super::*;
    use super::cheqd_keys::tests::KEY_ALIAS_WITH_BALANCE;
    use crate::utils::environment::EnvironmentUtils;

    const DID: &str = "did";
    const VERKEY: &str = "verkey";
    const ROLE: &str = "TRUSTEE";
    const MAX_GAS: &str = "1000000";
    const MAX_COIN: &str = "25000000";
    const AMOUNT: &str = "25000000";
    const MEMO: &str = "memo";

    mod cheqd_ledger {
        use super::*;
        use crate::commands::cheqd_keys::tests::get_key;
        use crate::utils::environment::EnvironmentUtils;
        use rstest::rstest;

        #[rstest(account_id,
        // Base account
        case("cheqd1rnr5jrt4exl0samwj0yegv99jeskl0hsxmcz96"),
        // Base vesting account
        case("cheqd1lkqddnapqvz2hujx2trpj7xj6c9hmuq7uhl0md"),
        // Continuous vesting account
        case("cheqd1353p46macvn444rupg2jstmx3tmz657yt9gl4l"),
        // Delayed Vesting account
        case("cheqd1njwu33lek5jt4kzlmljkp366ny4qpqusahpyrj"),
        // Periodic vesting account
        case("cheqd1uyngr0l3xtyj07js9sdew9mk50tqeq8lghhcfr"),
        )]
        pub fn get_account(account_id: &str) {
            let ctx = setup_with_wallet_and_cheqd_pool();
            {
                let cmd = get_account_command::new();
                let mut params = CommandParams::new();
                params.insert("address", account_id.to_string());
                cmd.execute(&ctx, &params).unwrap();
            }
            tear_down_with_wallet(&ctx);
        }

        #[test]
        pub fn create_did() {
            let ctx = setup_with_wallet_and_cheqd_pool();
            let (did, _) = create_new_did(&ctx);
            {
                let cmd = create_did_command::new();
                let mut params = CommandParams::new();
                params.insert("did", did.to_string());
                params.insert("key_alias", KEY_ALIAS_WITH_BALANCE.to_string());
                params.insert("max_gas", MAX_GAS.to_string());
                params.insert("max_coin", MAX_COIN.to_string());
                params.insert("denom", EnvironmentUtils::cheqd_denom());
                params.insert("memo", MEMO.to_string());
                cmd.execute(&ctx, &params).unwrap();
            }
            tear_down_with_wallet(&ctx);
        }

        #[test]
        #[ignore]
        pub fn update_did() {
            let ctx = setup_with_wallet_and_cheqd_pool();
            let (did, _) = create_new_did(&ctx);
            {
                let cmd = create_did_command::new();
                let mut params = CommandParams::new();
                params.insert("did", did.to_string());
                params.insert("version_id", "transaction hash".to_string());
                params.insert("key_alias", KEY_ALIAS_WITH_BALANCE.to_string());
                params.insert("max_gas", MAX_GAS.to_string());
                params.insert("max_coin", MAX_COIN.to_string());
                params.insert("denom", EnvironmentUtils::cheqd_denom());
                params.insert("memo", MEMO.to_string());
                cmd.execute(&ctx, &params).unwrap();
            }
            tear_down_with_wallet(&ctx);
        }

        #[test]
        #[ignore]
        pub fn get_did() {
            let ctx = setup_with_wallet_and_cheqd_pool();
            {
                let cmd = get_did_command::new();
                let mut params = CommandParams::new();
                params.insert("id", "9999999".to_string());
                cmd.execute(&ctx, &params).unwrap();
            }
            tear_down_with_wallet(&ctx);
        }

        #[test]
        #[ignore] // FIXME resore cheqd tests in CLI
        pub fn bank_send() {
            let ctx = setup_with_wallet_and_cheqd_pool();
            let key_info: Value = get_key(&ctx);
            let account_id = key_info["account_id"].as_str().unwrap().to_string();
            let key_alias = key_info["alias"].as_str().unwrap().to_string();
            {
                let cmd = bank_send_command::new();
                let mut params = CommandParams::new();
                params.insert("from", account_id.clone());
                params.insert("to", account_id);
                params.insert("amount", AMOUNT.to_string());
                params.insert("denom", EnvironmentUtils::cheqd_denom());
                params.insert("key_alias",  key_alias);
                params.insert("max_gas", MAX_GAS.to_string());
                params.insert("max_coin", MAX_COIN.to_string());
                params.insert("memo", MEMO.to_string());
                cmd.execute(&ctx, &params).unwrap();
            }
            tear_down_with_wallet(&ctx);
        }

        #[test]
        pub fn get_balance() {
            let ctx = setup_with_wallet_and_cheqd_pool();
            let key_info = get_key(&ctx);
            let account_id = key_info["account_id"].as_str().unwrap().to_string();
            {
                let cmd = get_balance_command::new();
                let mut params = CommandParams::new();
                params.insert("address", account_id);
                params.insert("denom", EnvironmentUtils::cheqd_denom());
                cmd.execute(&ctx, &params).unwrap();
            }
            tear_down_with_wallet(&ctx);
        }
    }

    fn did_info() -> String {
        json!({
            "ledger_type": "cheqd",
            "method_name": "testnet",
        }).to_string()
    }

    fn create_new_did(ctx: &CommandContext) -> (String, String) {
        let (wallet_handle, _) = get_opened_wallet(ctx).unwrap();
        Did::new(wallet_handle, &did_info()).unwrap()
    }
}
