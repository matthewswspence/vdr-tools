FROM ubuntu:16.04

RUN apt-get update && \
    apt-get install -y \
    openjdk-8-jdk \
    maven

ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64

RUN apt-get update && \
      apt-get install -y \
      software-properties-common

RUN apt-get update && \
      apt-get install -y \
      python3.5 \
      python3-pip \
      vim

RUN pip3 install -U pip

ARG libvdrtools_deb
RUN echo ${libvdrtools_deb}
ADD ${libvdrtools_deb} libvdrtools.deb

RUN dpkg -i libvdrtools.deb || apt-get install -y -f

RUN useradd -ms /bin/bash indy
USER indy
WORKDIR /home/indy
