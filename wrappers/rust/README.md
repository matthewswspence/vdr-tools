# vdrtools

[Libvdrtools](https://gitlab.com/evernym/verity/vdr-tools/) major artifact of the SDK is a C-callable library that provides the basic building blocks for the creation of applications on the top of Verifiable Data Registries like Hyperledger Indy, which provide a foundation for self-sovereign identity.

**vdrtools** is a library for assisting developers using LibIndy API.   

## Using vdrtools
- **vdrtools** does not include LibVdrTools. Install native "vdrtools" library:
	* Ubuntu:  https://repo.sovrin.org/lib/apt/xenial/
	* Windows: https://repo.sovrin.org/windows/libindy/
	
- Add **vdrtools** to Cargo.toml 
```
[dependencies]
vdrtools = "0.8.3"
```

# Note
This library is currently in experimental state.

# License
Released under Apache 2.0 and MIT.  See license files in git repo.