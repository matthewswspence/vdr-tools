use super::*;

use crate::{BString, CString, CommandHandle, Error, WalletHandle, CMutPtr ,CPtr};

extern "C" {
    pub fn vdr_builder_create(
        vdr_p: CMutPtr,
    ) -> Error;

    pub fn vdr_builder_register_indy_ledger(
        command_handle: CommandHandle,
        vdr: CPtr,
        namespace_list: CString,
        genesis_txn_data: CString,
        taa_config: CString,
        cb: Option<ResponseEmptyCB>,
    ) -> Error;

    #[cfg(feature = "cheqd")]
    pub fn vdr_builder_register_cheqd_ledger(
        command_handle: CommandHandle,
        vdr: CPtr,
        namespace_list: CString,
        chain_id: CString,
        node_addrs_list: CString,
        cb: Option<ResponseEmptyCB>,
    ) -> Error;

    pub fn vdr_builder_finalize(
        vdr_builder: CPtr,
        vdr_p: CMutPtr,
    ) -> Error;

    pub fn vdr_ping(
        command_handle: CommandHandle,
        vdr: CPtr,
        namespace_list: CString,
        cb: Option<ResponseStringCB>,
    ) -> Error;

    pub fn vdr_cleanup(
        command_handle: CommandHandle,
        vdr: CPtr,
        cb: Option<ResponseEmptyCB>,
    ) -> Error;

    pub fn vdr_resolve_did(
        command_handle: CommandHandle,
        vdr: CPtr,
        fqdid: CString,
        cb: Option<ResponseStringCB>,
    ) -> Error;

    pub fn vdr_resolve_did_with_cache(
        command_handle: CommandHandle,
        vdr: CPtr,
        wallet_handle: WalletHandle,
        fqdid: CString,
        cache_options: CString,
        cb: Option<ResponseStringCB>,
    ) -> Error;

    pub fn vdr_resolve_schema(
        command_handle: CommandHandle,
        vdr: CPtr,
        fqdid: CString,
        cb: Option<ResponseStringCB>,
    ) -> Error;

    pub fn vdr_resolve_schema_with_cache(
        command_handle: CommandHandle,
        vdr: CPtr,
        wallet_handle: WalletHandle,
        fqdid: CString,
        cache_options: CString,
        cb: Option<ResponseStringCB>,
    ) -> Error;

    pub fn vdr_resolve_cred_def(
        command_handle: CommandHandle,
        vdr: CPtr,
        fqcreddef: CString,
        cb: Option<ResponseStringCB>,
    ) -> Error;

    pub fn vdr_resolve_cred_def_with_cache(
        command_handle: CommandHandle,
        vdr: CPtr,
        wallet_handle: WalletHandle,
        fqcreddef: CString,
        cache_options: CString,
        cb: Option<ResponseStringCB>,
    ) -> Error;

    pub fn vdr_prepare_did(
        command_handle: CommandHandle,
        vdr: CPtr,
        txn_specific_params: CString,
        submitter_did: CString,
        endorser: CString,
        cb: Option<ResponsePreparedTxnCB>,
    ) -> Error;

    pub fn vdr_prepare_schema(
        command_handle: CommandHandle,
        vdr: CPtr,
        txn_specific_params: CString,
        submitter_did: CString,
        endorser: CString,
        cb: Option<ResponsePreparedTxnCB>,
    ) -> Error;

    pub fn vdr_prepare_cred_def(
        command_handle: CommandHandle,
        vdr: CPtr,
        txn_specific_params: CString,
        submitter_did: CString,
        endorser: CString,
        cb: Option<ResponsePreparedTxnCB>,
    ) -> Error;

    pub fn vdr_submit_txn(
        command_handle: CommandHandle,
        vdr: CPtr,
        namespace: CString,
        txn_bytes_raw: BString,
        txn_bytes_len: u32,
        signature_spec: CString,
        signature_raw: BString,
        signature_len: u32,
        endorsement: CString,
        cb: Option<ResponseStringCB>,
    ) -> Error;

    pub fn vdr_submit_raw_txn(
        command_handle: CommandHandle,
        vdr: CPtr,
        namespace: CString,
        txn_bytes_raw: BString,
        txn_bytes_len: u32,
        cb: Option<ResponseStringCB>,
    ) -> Error;

    pub fn vdr_submit_query(
        command_handle: CommandHandle,
        vdr: CPtr,
        namespace: CString,
        query: CString,
        cb: Option<ResponseStringCB>,
    ) -> Error;

    pub fn vdr_indy_endorse(
        command_handle: CommandHandle,
        wallet_handle: WalletHandle,
        endorsement_data: CString,
        signature_spec: CString,
        txn_bytes_to_sign_raw: BString,
        txn_bytes_to_sign_len: u32,
        cb: Option<ResponseStringCB>,
    ) -> Error;

    #[cfg(feature = "cheqd")]
    pub fn vdr_cheqd_endorse(
        command_handle: CommandHandle,
        wallet_handle: WalletHandle,
        endorsement_data: CString,
        signature_spec: CString,
        txn_bytes_to_sign_raw: BString,
        txn_bytes_to_sign_len: u32,
        signature_raw: BString,
        signature_len: u32,
        cb: Option<ResponseStringCB>,
    ) -> Error;

    #[cfg(feature = "cheqd")]
    pub fn vdr_prepare_cheqd_endorsement_data(
        command_handle: CommandHandle,
        vdr: CPtr,
        wallet_handle: WalletHandle,
        key_alias: CString,
        did: CString,
        txn_bytes_raw: BString,
        txn_bytes_len: u32,
        txn_signature_raw: BString,
        txn_signature_len: u32,
        gas_price: u64,
        memo: CString,
        cb: Option<ResponseStringCB>,
    ) -> Error;

}
