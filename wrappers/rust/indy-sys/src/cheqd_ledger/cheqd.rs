use crate::{CString, CommandHandle, Error, WalletHandle, BString};
use crate::{ResponseStringCB, ResponseSliceCB};

extern "C" {
    pub fn cheqd_ledger_cheqd_sign_msg_write_request(
        command_handle: CommandHandle,
        wallet_handle: WalletHandle,
        fully_did: CString,
        request_bytes: BString,
        request_len: u32,
        cb: Option<ResponseSliceCB>,
    ) -> Error;

    pub fn cheqd_ledger_cheqd_build_msg_create_did(
        command_handle: CommandHandle,
        did: CString,
        verkey: CString,
        cb: Option<ResponseSliceCB>,
    ) -> Error;

    pub fn cheqd_ledger_cheqd_parse_msg_create_did_resp(
        command_handle: CommandHandle,
        commit_resp: CString,
        cb: Option<ResponseStringCB>,
    ) -> Error;

    pub fn cheqd_ledger_cheqd_build_msg_update_did(
        command_handle: CommandHandle,
        did: CString,
        verkey: CString,
        version_id: CString,
        cb: Option<ResponseSliceCB>,
    ) -> Error;

    pub fn cheqd_ledger_cheqd_parse_msg_update_did_resp(
        command_handle: CommandHandle,
        commit_resp: CString,
        cb: Option<ResponseStringCB>,
    ) -> Error;

    pub fn cheqd_ledger_cheqd_build_query_get_did(
        command_handle: CommandHandle,
        did: CString,
        cb: Option<ResponseStringCB>,
    ) -> Error;

    pub fn cheqd_ledger_cheqd_parse_query_get_did_resp(
        command_handle: CommandHandle,
        query_resp: CString,
        cb: Option<ResponseStringCB>,
    ) -> Error;
}
