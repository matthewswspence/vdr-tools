use crate::{ErrorCode, IndyError};

use std::ffi::CString;

use ffi::cheqd_ledger::tx;
use futures::Future;

use crate::ffi::{ResponseStringCB};

use crate::utils::callbacks::{ClosureHandler, ResultHandler};
use crate::CommandHandle;


pub fn build_query_get_tx_by_hash(hash: &str) -> Box<dyn Future<Item = String, Error = IndyError>> {
    let (receiver, command_handle, cb) = ClosureHandler::cb_ec_string();

    let err = _build_query_get_tx_by_hash(command_handle, hash, cb);

    ResultHandler::str(command_handle, err, receiver)
}

fn _build_query_get_tx_by_hash(
    command_handle: CommandHandle,
    hash: &str,
    cb: Option<ResponseStringCB>,
) -> ErrorCode {
    let hash = c_str!(hash);

    ErrorCode::from(unsafe {
        tx::cheqd_ledger_tx_build_query_get_tx_by_hash(command_handle, hash.as_ptr(), cb)
    })
}

pub fn parse_query_get_tx_by_hash_resp(
    query_resp: &str,
) -> Box<dyn Future<Item = String, Error = IndyError>> {
    let (receiver, command_handle, cb) = ClosureHandler::cb_ec_string();

    let err = _parse_query_get_tx_by_hash_resp(command_handle, query_resp, cb);

    ResultHandler::str(command_handle, err, receiver)
}

fn _parse_query_get_tx_by_hash_resp(
    command_handle: CommandHandle,
    query_resp: &str,
    cb: Option<ResponseStringCB>,
) -> ErrorCode {
    let query_resp = c_str!(query_resp);

    ErrorCode::from(unsafe {
        tx::cheqd_ledger_cheqd_parse_query_get_tx_by_hash_resp(
            command_handle,
            query_resp.as_ptr(),
            cb,
        )
    })
}

pub fn build_query_simulate(
    tx: &[u8],
) -> Box<dyn Future<Item=String, Error=IndyError>> {
    let (receiver, command_handle, cb) = ClosureHandler::cb_ec_string();

    let err = _build_query_simulate(command_handle, tx, cb);

    ResultHandler::str(command_handle, err, receiver)
}

fn _build_query_simulate(
    command_handle: CommandHandle,
    tx: &[u8],
    cb: Option<ResponseStringCB>,
) -> ErrorCode {
    ErrorCode::from(unsafe {
        tx::cheqd_ledger_tx_build_query_simulate(
            command_handle,
            tx.as_ptr() as *const u8,
            tx.len() as u32,
            cb,
        )
    })
}

pub fn parse_query_simulate_resp(
    query_resp: &str,
) -> Box<dyn Future<Item = String, Error = IndyError>> {
    let (receiver, command_handle, cb) = ClosureHandler::cb_ec_string();

    let err = _parse_query_simulate_resp(command_handle, query_resp, cb);

    ResultHandler::str(command_handle, err, receiver)
}

fn _parse_query_simulate_resp(
    command_handle: CommandHandle,
    query_resp: &str,
    cb: Option<ResponseStringCB>,
) -> ErrorCode {
    let query_resp = c_str!(query_resp);

    ErrorCode::from(unsafe {
        tx::cheqd_ledger_tx_parse_query_simulate_resp(
            command_handle,
            query_resp.as_ptr(),
            cb,
        )
    })
}


