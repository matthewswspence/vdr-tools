use std::ffi::CString;
use std::ptr::null;
use libc::c_void;
use std::{
    collections::HashMap,
};

use futures::Future;
use async_std::sync::{Arc, Mutex, RwLock};
use async_trait::async_trait;

use crate::{ErrorCode, IndyError};
use crate::{CommandHandle, WalletHandle};
use crate::ffi::{ResponseEmptyCB, ResponsePreparedTxnCB, ResponseStringCB};
use crate::ffi::vdr;
use crate::utils::callbacks::{ClosureHandler, ResultHandler};

pub type VDRBuilder = Arc<Mutex<VDRBuilderObject>>;

pub struct VDRBuilderObject {
    _namespaces: HashMap<String, Arc<RwLock<dyn Ledger + 'static>>>,
}

pub struct VDR {
    _namespaces: HashMap<String, Arc<RwLock<dyn Ledger + 'static>>>,
}

#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Clone)]
pub struct PingStatus {
    pub code: PingStatusCodes,
    pub message: String,
}

#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Clone)]
pub enum PingStatusCodes {
    SUCCESS,
    FAIL,
}

#[async_trait]
pub(crate) trait Ledger: Send + Sync {
    // general
    async fn ping(&self) -> Result<PingStatus, IndyError>;
    async fn submit_txn(&self, txn_bytes: &[u8], signature: &[u8], endorsement: Option<&str>) -> Result<String, IndyError>;
    async fn submit_raw_txn(&self, txn_bytes: &[u8]) -> Result<String, IndyError>;
    async fn submit_query(&self, request: &str) -> Result<String, IndyError>;
    async fn cleanup(&self) -> Result<(), IndyError>;

    // did builders + parser
    async fn build_did_request(&self, txn_params: &str, submitter_did: &str, endorser: Option<&str>) -> Result<(Vec<u8>, Vec<u8>), IndyError>;
    async fn build_resolve_did_request(&self, id: &str) -> Result<String, IndyError>;
    async fn parse_resolve_did_response(&self, response: &str) -> Result<String, IndyError>;

    // schema builders + parser
    async fn build_schema_request(&self, txn_params: &str, submitter_did: &str, endorser: Option<&str>) -> Result<(Vec<u8>, Vec<u8>), IndyError>;
    async fn build_resolve_schema_request(&self, id: &str) -> Result<String, IndyError>;
    async fn parse_resolve_schema_response(&self, response: &str) -> Result<String, IndyError>;

    // creddef builders + parser
    async fn build_cred_def_request(&self, txn_params: &str, submitter_did: &str, endorser: Option<&str>) -> Result<(Vec<u8>, Vec<u8>), IndyError>;
    async fn build_resolve_cred_def_request(&self, id: &str) -> Result<String, IndyError>;
    async fn parse_resolve_cred_def_response(&self, response: &str) -> Result<String, IndyError>;
}

pub fn vdr_builder_create() -> Result<VDRBuilder, IndyError> {
    let mut vdr_builder_p: *const c_void = null();

    let err = _vdr_create(&mut vdr_builder_p);

    if err != ErrorCode::Success {
        return Err(IndyError::new(ErrorCode::CommonInvalidState));
    }

    let vdr = unsafe { Box::from_raw(vdr_builder_p as *mut VDRBuilder) };

    return Ok(*vdr);
}

fn _vdr_create(vdr_builder_p: &mut *const c_void) -> ErrorCode {
    ErrorCode::from(unsafe {
        vdr::vdr_builder_create(vdr_builder_p)
    })
}

pub fn vdr_builder_register_indy_ledger(vdr_builder: &VDRBuilder, namespace_list: &str, genesis_txn_data: &str, taa_config: Option<&str>) -> Box<dyn Future<Item=(), Error=IndyError>> {
    let (receiver, command_handle, cb) = ClosureHandler::cb_ec();

    let err = _vdr_register_indy_ledger(command_handle, vdr_builder, namespace_list, genesis_txn_data, taa_config, cb);

    ResultHandler::empty(command_handle, err, receiver)
}

fn _vdr_register_indy_ledger(command_handle: CommandHandle, vdr_builder: &VDRBuilder, namespace_list: &str, genesis_txn_data: &str, taa_config: Option<&str>, cb: Option<ResponseEmptyCB>) -> ErrorCode {
    let vdr_builder_p = vdr_builder as *const _ as *mut c_void;
    let namespace_list = c_str!(namespace_list);
    let genesis_txn_data = c_str!(genesis_txn_data);
    let taa_config_str = opt_c_str!(taa_config);

    ErrorCode::from(unsafe {
        vdr::vdr_builder_register_indy_ledger(command_handle, vdr_builder_p, namespace_list.as_ptr(), genesis_txn_data.as_ptr(), opt_c_ptr!(taa_config, taa_config_str), cb)
    })
}

#[cfg(feature = "cheqd")]
pub fn vdr_builder_register_cheqd_ledger(vdr_builder: &VDRBuilder, namespace_list: &str, chain_id: &str, node_addrs_list: &str) -> Box<dyn Future<Item=(), Error=IndyError>> {
    let (receiver, command_handle, cb) = ClosureHandler::cb_ec();

    let err = _vdr_register_cheqd_ledger(command_handle, vdr_builder, namespace_list, chain_id, node_addrs_list, cb);

    ResultHandler::empty(command_handle, err, receiver)
}

#[cfg(feature = "cheqd")]
fn _vdr_register_cheqd_ledger(command_handle: CommandHandle, vdr_builder: &VDRBuilder, namespace_list: &str, chain_id: &str, node_addrs_list: &str, cb: Option<ResponseEmptyCB>) -> ErrorCode {
    let vdr_builder_p = vdr_builder as *const _ as *mut c_void;
    let namespace_list = c_str!(namespace_list);
    let chain_id = c_str!(chain_id);
    let node_addrs_list = c_str!(node_addrs_list);

    ErrorCode::from(unsafe {
        vdr::vdr_builder_register_cheqd_ledger(command_handle, vdr_builder_p, namespace_list.as_ptr(), chain_id.as_ptr(), node_addrs_list.as_ptr(), cb)
    })
}

pub fn vdr_builder_finalize(vdr_builder: VDRBuilder) -> Result<VDR, IndyError> {
    let mut vdr_p: *const c_void = null();

    let err = _vdr_finalize(vdr_builder, &mut vdr_p);

    if err != ErrorCode::Success {
        return Err(IndyError::new(ErrorCode::CommonInvalidState));
    }

    let vdr = unsafe { Box::from_raw(vdr_p as *mut VDR) };

    return Ok(*vdr);
}

fn _vdr_finalize(vdr_builder: VDRBuilder, vdr_p: &mut *const c_void) -> ErrorCode {
    let vdr_builder_p = Box::into_raw(Box::new(vdr_builder)) as *const c_void;
    ErrorCode::from(unsafe {
        vdr::vdr_builder_finalize(vdr_builder_p, vdr_p)
    })
}

pub fn ping(vdr: &VDR, namespace_list: &str) -> Box<dyn Future<Item=String, Error=IndyError>> {
    let (receiver, command_handle, cb) = ClosureHandler::cb_ec_string();

    let err = _vdr_ping(command_handle, vdr, namespace_list, cb);

    ResultHandler::str(command_handle, err, receiver)
}

fn _vdr_ping(command_handle: CommandHandle, vdr: &VDR, namespace_list: &str, cb: Option<ResponseStringCB>) -> ErrorCode {
    let vdr_p = vdr as *const _ as *const c_void;
    let namespace_list = c_str!(namespace_list);

    ErrorCode::from(unsafe {
        vdr::vdr_ping(command_handle, vdr_p, namespace_list.as_ptr(), cb)
    })
}

pub fn cleanup(vdr: VDR) -> Box<dyn Future<Item=(), Error=IndyError>> {
    let (receiver, command_handle, cb) = ClosureHandler::cb_ec();

    let err = _vdr_cleanup(command_handle, vdr, cb);

    ResultHandler::empty(command_handle, err, receiver)
}

fn _vdr_cleanup(command_handle: CommandHandle, vdr: VDR, cb: Option<ResponseEmptyCB>) -> ErrorCode {
    let vdr_p = Box::into_raw(Box::new(vdr)) as *const c_void;

    ErrorCode::from(unsafe {
        vdr::vdr_cleanup(command_handle, vdr_p, cb)
    })
}


pub fn resolve_did(vdr: &VDR, fqdid: &str) -> Box<dyn Future<Item=String, Error=IndyError>> {
    let (receiver, command_handle, cb) = ClosureHandler::cb_ec_string();

    let err = _vdr_resolve_did(command_handle, vdr, fqdid, cb);

    ResultHandler::str(command_handle, err, receiver)
}

fn _vdr_resolve_did(command_handle: CommandHandle, vdr: &VDR, fqdid: &str, cb: Option<ResponseStringCB>) -> ErrorCode {
    let vdr_p = vdr as *const _ as *const c_void;
    let fqdid = c_str!(fqdid);

    ErrorCode::from(unsafe {
        vdr::vdr_resolve_did(command_handle, vdr_p, fqdid.as_ptr(), cb)
    })
}

pub fn resolve_did_with_cache(vdr: &VDR, wallet_handle: WalletHandle, fqdid: &str, cache_options: &str) -> Box<dyn Future<Item=String, Error=IndyError>> {
    let (receiver, command_handle, cb) = ClosureHandler::cb_ec_string();

    let err = _resolve_did_with_cache(command_handle, vdr, wallet_handle, fqdid, cache_options, cb);

    ResultHandler::str(command_handle, err, receiver)
}

fn _resolve_did_with_cache(command_handle: CommandHandle, vdr: &VDR, wallet_handle: WalletHandle, fqdid: &str, cache_options: &str, cb: Option<ResponseStringCB>) -> ErrorCode {
    let vdr_p = vdr as *const _ as *const c_void;
    let fqdid = c_str!(fqdid);
    let cache_options = c_str!(cache_options);

    ErrorCode::from(unsafe {
        vdr::vdr_resolve_did_with_cache(command_handle, vdr_p, wallet_handle, fqdid.as_ptr(), cache_options.as_ptr(), cb)
    })
}

pub fn resolve_schema(vdr: &VDR, fqschema: &str) -> Box<dyn Future<Item=String, Error=IndyError>> {
    let (receiver, command_handle, cb) = ClosureHandler::cb_ec_string();

    let err = _vdr_resolve_schema(command_handle, vdr, fqschema, cb);

    ResultHandler::str(command_handle, err, receiver)
}

fn _vdr_resolve_schema(command_handle: CommandHandle, vdr: &VDR, fqschema: &str, cb: Option<ResponseStringCB>) -> ErrorCode {
    let vdr_p = vdr as *const _ as *const c_void;
    let fqschema = c_str!(fqschema);

    ErrorCode::from(unsafe {
        vdr::vdr_resolve_schema(command_handle, vdr_p, fqschema.as_ptr(), cb)
    })
}

pub fn resolve_schema_with_cache(vdr: &VDR, wallet_handle: WalletHandle, fqschema: &str, cache_options: &str) -> Box<dyn Future<Item=String, Error=IndyError>> {
    let (receiver, command_handle, cb) = ClosureHandler::cb_ec_string();

    let err = _resolve_schema_with_cache(command_handle, vdr, wallet_handle, fqschema, cache_options, cb);

    ResultHandler::str(command_handle, err, receiver)
}

fn _resolve_schema_with_cache(command_handle: CommandHandle, vdr: &VDR, wallet_handle: WalletHandle, fqschema: &str, cache_options: &str, cb: Option<ResponseStringCB>) -> ErrorCode {
    let vdr_p = vdr as *const _ as *const c_void;
    let fqschema = c_str!(fqschema);
    let cache_options = c_str!(cache_options);

    ErrorCode::from(unsafe {
        vdr::vdr_resolve_schema_with_cache(command_handle, vdr_p, wallet_handle, fqschema.as_ptr(), cache_options.as_ptr(), cb)
    })
}

pub fn resolve_cred_def(vdr: &VDR, fqcreddef: &str) -> Box<dyn Future<Item=String, Error=IndyError>> {
    let (receiver, command_handle, cb) = ClosureHandler::cb_ec_string();

    let err = _vdr_resolve_cred_def(command_handle, vdr, fqcreddef, cb);

    ResultHandler::str(command_handle, err, receiver)
}

fn _vdr_resolve_cred_def(command_handle: CommandHandle, vdr: &VDR, fqcreddef: &str, cb: Option<ResponseStringCB>) -> ErrorCode {
    let vdr_p = vdr as *const _ as *const c_void;
    let fqcreddef = c_str!(fqcreddef);

    ErrorCode::from(unsafe {
        vdr::vdr_resolve_cred_def(command_handle, vdr_p, fqcreddef.as_ptr(), cb)
    })
}

pub fn resolve_cred_def_with_cache(vdr: &VDR, wallet_handle: WalletHandle, fqcreddef: &str, cache_options: &str) -> Box<dyn Future<Item=String, Error=IndyError>> {
    let (receiver, command_handle, cb) = ClosureHandler::cb_ec_string();

    let err = _resolve_cred_def_with_cache(command_handle, vdr, wallet_handle, fqcreddef, cache_options, cb);

    ResultHandler::str(command_handle, err, receiver)
}

fn _resolve_cred_def_with_cache(command_handle: CommandHandle, vdr: &VDR, wallet_handle: WalletHandle, fqcreddef: &str, cache_options: &str, cb: Option<ResponseStringCB>) -> ErrorCode {
    let vdr_p = vdr as *const _ as *const c_void;
    let fqcreddef = c_str!(fqcreddef);
    let cache_options = c_str!(cache_options);

    ErrorCode::from(unsafe {
        vdr::vdr_resolve_cred_def_with_cache(command_handle, vdr_p, wallet_handle, fqcreddef.as_ptr(), cache_options.as_ptr(), cb)
    })
}

pub fn prepare_did(vdr: &VDR, txn_specific_params: &str, submitter_did: &str, endorser: Option<&str>) -> Box<dyn Future<Item=(String, Vec<u8>, String, Vec<u8>, Option<String>), Error=IndyError>> {
    let (receiver, command_handle, cb) = ClosureHandler::cb_ec_preparedtxnhandle();

    let err = _vdr_prepare_did(command_handle, vdr, txn_specific_params, submitter_did, endorser, cb);

    ResultHandler::preparedtxnhandle(command_handle, err, receiver)
}

fn _vdr_prepare_did(command_handle: CommandHandle, vdr: &VDR, txn_specific_params: &str, submitter_did: &str, endorser: Option<&str>, cb: Option<ResponsePreparedTxnCB>) -> ErrorCode {
    let vdr_p = vdr as *const _ as *const c_void;
    let txn_specific_params = c_str!(txn_specific_params);
    let submitter_did = c_str!(submitter_did);
    let endorser_str = opt_c_str!(endorser);

    ErrorCode::from(unsafe {
        vdr::vdr_prepare_did(command_handle, vdr_p, txn_specific_params.as_ptr(), submitter_did.as_ptr(), opt_c_ptr!(endorser, endorser_str), cb)
    })
}

pub fn prepare_schema(vdr: &VDR, txn_specific_params: &str, submitter_did: &str, endorser: Option<&str>) -> Box<dyn Future<Item=(String, Vec<u8>, String, Vec<u8>, Option<String>), Error=IndyError>> {
    let (receiver, command_handle, cb) = ClosureHandler::cb_ec_preparedtxnhandle();

    let err = _vdr_prepare_schema(command_handle, vdr, txn_specific_params, submitter_did, endorser, cb);

    ResultHandler::preparedtxnhandle(command_handle, err, receiver)
}

fn _vdr_prepare_schema(command_handle: CommandHandle, vdr: &VDR, txn_specific_params: &str, submitter_did: &str, endorser: Option<&str>, cb: Option<ResponsePreparedTxnCB>) -> ErrorCode {
    let vdr_p = vdr as *const _ as *const c_void;
    let txn_specific_params = c_str!(txn_specific_params);
    let submitter_did = c_str!(submitter_did);
    let endorser_str = opt_c_str!(endorser);

    ErrorCode::from(unsafe {
        vdr::vdr_prepare_schema(command_handle, vdr_p, txn_specific_params.as_ptr(), submitter_did.as_ptr(), opt_c_ptr!(endorser, endorser_str), cb)
    })
}

pub fn prepare_cred_def(vdr: &VDR, txn_specific_params: &str, submitter_did: &str, endorser: Option<&str>) -> Box<dyn Future<Item=(String, Vec<u8>, String, Vec<u8>, Option<String>), Error=IndyError>> {
    let (receiver, command_handle, cb) = ClosureHandler::cb_ec_preparedtxnhandle();

    let err = _vdr_prepare_cred_def(command_handle, vdr, txn_specific_params, submitter_did, endorser, cb);

    ResultHandler::preparedtxnhandle(command_handle, err, receiver)
}

fn _vdr_prepare_cred_def(command_handle: CommandHandle, vdr: &VDR, txn_specific_params: &str, submitter_did: &str, endorser: Option<&str>, cb: Option<ResponsePreparedTxnCB>) -> ErrorCode {
    let vdr_p = vdr as *const _ as *const c_void;
    let txn_specific_params = c_str!(txn_specific_params);
    let submitter_did = c_str!(submitter_did);
    let endorser_str = opt_c_str!(endorser);

    ErrorCode::from(unsafe {
        vdr::vdr_prepare_cred_def(command_handle, vdr_p, txn_specific_params.as_ptr(), submitter_did.as_ptr(), opt_c_ptr!(endorser, endorser_str), cb)
    })
}

pub fn submit_txn(
    vdr: &VDR,
    namespace: &str,
    txn_bytes: &[u8],
    signature_spec: &str,
    signature: &[u8],
    endorsement: Option<&str>,
) -> Box<dyn Future<Item=String, Error=IndyError>> {
    let (receiver, command_handle, cb) = ClosureHandler::cb_ec_string();

    let err = _vdr_submit_txn(command_handle, vdr, namespace, txn_bytes, signature_spec, signature, endorsement, cb);

    ResultHandler::str(command_handle, err, receiver)
}

fn _vdr_submit_txn(
    command_handle: CommandHandle,
    vdr: &VDR,
    namespace: &str,
    txn_bytes: &[u8],
    signature_spec: &str,
    signature: &[u8],
    endorsement: Option<&str>,
    cb: Option<ResponseStringCB>,
) -> ErrorCode {
    let vdr_p = vdr as *const _ as *const c_void;
    let namespace = c_str!(namespace);
    let signature_spec = c_str!(signature_spec);
    let endorsement_str = opt_c_str!(endorsement);

    ErrorCode::from(unsafe {
        vdr::vdr_submit_txn(command_handle, vdr_p, namespace.as_ptr(),
                            txn_bytes.as_ptr() as *const u8, txn_bytes.len() as u32,
                            signature_spec.as_ptr(), signature.as_ptr() as *const u8, signature.len() as u32,
                            opt_c_ptr!(endorsement, endorsement_str), cb)
    })
}

pub fn submit_raw_txn(
    vdr: &VDR,
    namespace: &str,
    txn_bytes: &[u8],
) -> Box<dyn Future<Item=String, Error=IndyError>> {
    let (receiver, command_handle, cb) = ClosureHandler::cb_ec_string();

    let err = _vdr_submit_raw_txn(command_handle, vdr, namespace, txn_bytes, cb);

    ResultHandler::str(command_handle, err, receiver)
}

fn _vdr_submit_raw_txn(
    command_handle: CommandHandle,
    vdr: &VDR,
    namespace: &str,
    txn_bytes: &[u8],
    cb: Option<ResponseStringCB>,
) -> ErrorCode {
    let vdr_p = vdr as *const _ as *const c_void;
    let namespace = c_str!(namespace);

    ErrorCode::from(unsafe {
        vdr::vdr_submit_raw_txn(command_handle, vdr_p, namespace.as_ptr(),
                                txn_bytes.as_ptr() as *const u8, txn_bytes.len() as u32, cb)
    })
}

pub fn submit_query(
    vdr: &VDR,
    namespace: &str,
    query: &str,
) -> Box<dyn Future<Item=String, Error=IndyError>> {
    let (receiver, command_handle, cb) = ClosureHandler::cb_ec_string();

    let err = _submit_query(command_handle, vdr, namespace, query, cb);

    ResultHandler::str(command_handle, err, receiver)
}

fn _submit_query(
    command_handle: CommandHandle,
    vdr: &VDR,
    namespace: &str,
    query: &str,
    cb: Option<ResponseStringCB>,
) -> ErrorCode {
    let vdr_p = vdr as *const _ as *const c_void;
    let namespace = c_str!(namespace);
    let query = c_str!(query);

    ErrorCode::from(unsafe {
        vdr::vdr_submit_query(command_handle, vdr_p, namespace.as_ptr(), query.as_ptr(), cb)
    })
}

pub fn indy_endorse(
    wallet_handle: WalletHandle,
    endorsement_data: &str,
    signature_spec: &str,
    txn_bytes_to_sign: &[u8],
) -> Box<dyn Future<Item=String, Error=IndyError>> {
    let (receiver, command_handle, cb) = ClosureHandler::cb_ec_string();

    let err = _vdr_indy_endorse(command_handle, wallet_handle, endorsement_data, signature_spec, txn_bytes_to_sign, cb);

    ResultHandler::str(command_handle, err, receiver)
}

fn _vdr_indy_endorse(
    command_handle: CommandHandle,
    wallet_handle: WalletHandle,
    endorsement_data: &str,
    signature_spec: &str,
    txn_bytes_to_sign: &[u8],
    cb: Option<ResponseStringCB>,
) -> ErrorCode {
    let endorsement_data = c_str!(endorsement_data);
    let signature_spec = c_str!(signature_spec);

    ErrorCode::from(unsafe {
        vdr::vdr_indy_endorse(command_handle, wallet_handle, endorsement_data.as_ptr(), signature_spec.as_ptr(),
                              txn_bytes_to_sign.as_ptr() as *const u8, txn_bytes_to_sign.len() as u32, cb)
    })
}

#[cfg(feature = "cheqd")]
pub fn cheqd_endorse(
    wallet_handle: WalletHandle,
    endorsement_data: &str,
    signature_spec: &str,
    txn_bytes_to_sign: &[u8],
    signature: &[u8],
) -> Box<dyn Future<Item=String, Error=IndyError>> {
    let (receiver, command_handle, cb) = ClosureHandler::cb_ec_string();

    let err = _vdr_cheqd_endorse(command_handle, wallet_handle, endorsement_data, signature_spec, txn_bytes_to_sign, signature, cb);

    ResultHandler::str(command_handle, err, receiver)
}

#[cfg(feature = "cheqd")]
fn _vdr_cheqd_endorse(
    command_handle: CommandHandle,
    wallet_handle: WalletHandle,
    endorsement_data: &str,
    signature_spec: &str,
    txn_bytes_to_sign: &[u8],
    signature: &[u8],
    cb: Option<ResponseStringCB>,
) -> ErrorCode {
    let endorsement_data = c_str!(endorsement_data);
    let signature_spec = c_str!(signature_spec);

    ErrorCode::from(unsafe {
        vdr::vdr_cheqd_endorse(command_handle, wallet_handle, endorsement_data.as_ptr(), signature_spec.as_ptr(),
                               txn_bytes_to_sign.as_ptr() as *const u8, txn_bytes_to_sign.len() as u32,
                               signature.as_ptr() as *const u8, signature.len() as u32,
                               cb)
    })
}

#[cfg(feature = "cheqd")]
pub fn prepare_cheqd_endorsement_data(
    vdr: &VDR,
    wallet_handle: WalletHandle,
    key_alias: &str,
    did: &str,
    tx_bytes: &[u8],
    txn_signature: &[u8],
    gas_price: u64,
    memo: &str,
) -> Box<dyn Future<Item=String, Error=IndyError>> {
    let (receiver, command_handle, cb) = ClosureHandler::cb_ec_string();

    let err = _prepare_cheqd_endorsement_data(command_handle, vdr, wallet_handle, key_alias, did, tx_bytes, txn_signature, gas_price, memo, cb);

    ResultHandler::str(command_handle, err, receiver)
}

#[cfg(feature = "cheqd")]
fn _prepare_cheqd_endorsement_data(
    command_handle: CommandHandle,
    vdr: &VDR,
    wallet_handle: WalletHandle,
    key_alias: &str,
    did: &str,
    tx_bytes: &[u8],
    txn_signature: &[u8],
    gas_price: u64,
    memo: &str,
    cb: Option<ResponseStringCB>,
) -> ErrorCode {
    let vdr_p = vdr as *const _ as *const c_void;
    let did = c_str!(did);
    let key_alias = c_str!(key_alias);
    let memo = c_str!(memo);

    ErrorCode::from(unsafe {
        vdr::vdr_prepare_cheqd_endorsement_data(command_handle, vdr_p, wallet_handle, key_alias.as_ptr(),did.as_ptr(),
                                                tx_bytes.as_ptr() as *const u8, tx_bytes.len() as u32,
                                                txn_signature.as_ptr() as *const u8, txn_signature.len() as u32,
                                                gas_price, memo.as_ptr(), cb)
    })
}
