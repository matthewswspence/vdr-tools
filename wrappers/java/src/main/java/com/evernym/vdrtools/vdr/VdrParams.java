package com.evernym.vdrtools.vdr;

public interface VdrParams {
    class TaaConfig {
        private final String text;
        private final String version;
        private final String taaDigest;
        private final String accMechType;
        private final String time;

        public TaaConfig(String text, String version, String accMechType, String time) {
            this.text = text;
            this.version = version;
            this.taaDigest = null;
            this.accMechType = accMechType;
            this.time = time;
        }

        public TaaConfig(String taaDigest, String accMechType, String time) {
            this.text = null;
            this.version = null;
            this.taaDigest = taaDigest;
            this.accMechType = accMechType;
            this.time = time;
        }

        public String getText() {
            return text;
        }

        public String getVersion() {
            return version;
        }

        public String getTaaDigest() {
            return taaDigest;
        }

        public String getAccMechType() {
            return accMechType;
        }

        public String getTime() {
            return time;
        }
    }

    class CacheOptions {
        private final Boolean noCache;
        private final Boolean noUpdate;
        private final Boolean noStore;
        private final Integer minFresh;

        public CacheOptions(Boolean noCache, Boolean noUpdate, Boolean noStore, Integer minFresh) {
            this.noCache = noCache;
            this.noUpdate = noUpdate;
            this.noStore = noStore;
            this.minFresh = minFresh;
        }

        public Boolean getNoCache() {
            return noCache;
        }

        public Boolean getNoUpdate() {
            return noUpdate;
        }

        public Boolean getNoStore() {
            return noStore;
        }

        public Integer getMinFresh() {
            return minFresh;
        }
    }

}