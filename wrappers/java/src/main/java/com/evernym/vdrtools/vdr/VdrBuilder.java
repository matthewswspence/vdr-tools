package com.evernym.vdrtools.vdr;

import com.evernym.vdrtools.*;
import com.sun.jna.Callback;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.PointerByReference;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public class VdrBuilder extends IndyJava.API {

    private final Pointer pointer;

    private VdrBuilder(Pointer pointer) {
        this.pointer = pointer;
    }

    public Pointer getPointer() {
        return pointer;
    }

    private static Callback voidCb = new Callback() {

        @SuppressWarnings({"unused", "unchecked"})
        public void callback(int xcommand_handle, int err) {

            CompletableFuture<Void> future = (CompletableFuture<Void>) removeFuture(xcommand_handle);
            if (!checkResult(future, err)) return;

            Void result = null;
            future.complete(result);
        }
    };

    public static VdrBuilder create() throws IndyException {
        PointerByReference vdrBuilderPBR = new PointerByReference();

        int result = LibIndy.api.vdr_builder_create(vdrBuilderPBR);
        checkResult(result);

        return new VdrBuilder(vdrBuilderPBR.getValue());
    }

    public CompletableFuture<Void> registerIndyLedger(
            List<String> namespaceList,
            String genesisTxnData,
            VdrParams.TaaConfig taaConfig
    ) {
        CompletableFuture<Void> future = new CompletableFuture<>();

        try {
            ParamGuard.notNull(namespaceList, "namespaceList");
            ParamGuard.notNull(genesisTxnData, "genesisTxnData");
        } catch (Exception ex) {
            future.completeExceptionally(ex);
            return future;
        }

        int commandHandle = addFuture(future);

        String namespaces = new JSONArray(namespaceList).toString();

        String taa = null;
        if (taaConfig != null) {
            JSONObject json = new JSONObject(taaConfig);
            taa = json.toString();
        }

        int result = LibIndy.api.vdr_builder_register_indy_ledger(
                commandHandle,
                this.getPointer(),
                namespaces,
                genesisTxnData,
                taa,
                voidCb
        );

        checkResult(future, result);

        return future;
    }

    public CompletableFuture<Void> registerCheqdLedger(
            List<String> namespaceList,
            String chainId,
            String nodeAddrsList
    ) {
        CompletableFuture<Void> future = new CompletableFuture<>();

        try {
            ParamGuard.notNull(namespaceList, "namespaceList");
            ParamGuard.notNull(chainId, "chainId");
            ParamGuard.notNull(nodeAddrsList, "nodeAddrsList");
        } catch (Exception ex) {
            future.completeExceptionally(ex);
            return future;
        }

        int commandHandle = addFuture(future);

        String namespaces = new JSONArray(namespaceList).toString();

        int result = LibIndy.api.vdr_builder_register_cheqd_ledger(
                commandHandle,
                this.getPointer(),
                namespaces,
                chainId,
                nodeAddrsList,
                voidCb
        );

        checkResult(future, result);

        return future;
    }

    public VDR build() throws IndyException {
        PointerByReference vdrPBR = new PointerByReference();

        int result = LibIndy.api.vdr_builder_finalize(this.getPointer(), vdrPBR);

        checkResult(result);

        return new VDR(vdrPBR.getValue());
    }
}
