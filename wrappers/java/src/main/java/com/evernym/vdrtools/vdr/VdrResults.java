package com.evernym.vdrtools.vdr;

import com.evernym.vdrtools.IndyJava;

public interface VdrResults {

    class PreparedTxnResult extends IndyJava.Result {
        private final String namespace;
        private final String signatureSpec;
        private final byte[] txnBytes;
        private final byte[] bytesToSign;
        private final String endorsementSpec;

        public PreparedTxnResult(String namespace, byte[] txnBytes, String signatureSpec, byte[] bytesToSign, String endorsementSpec) {
            this.namespace = namespace;
            this.signatureSpec = signatureSpec;
            this.txnBytes = txnBytes;
            this.bytesToSign = bytesToSign;
            this.endorsementSpec = endorsementSpec;
        }

        public String getNamespace() {
            return namespace;
        }

        public String getSignatureSpec() {
            return signatureSpec;
        }

        public byte[] getTxnBytes() {
            return txnBytes;
        }

        public byte[] getBytesToSign() {
            return bytesToSign;
        }

        public String getEndorsementSpec() {
            return endorsementSpec;
        }
    }

    class PingResult extends IndyJava.Result {
        private final String code; // todo enum?
        private final String message;

        public PingResult(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }

        public boolean isSuccessful() {
            return "SUCCESS".equals(this.message);
        }

    }
}
