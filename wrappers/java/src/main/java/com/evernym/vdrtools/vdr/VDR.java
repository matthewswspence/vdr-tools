package com.evernym.vdrtools.vdr;

import com.evernym.vdrtools.*;
import com.evernym.vdrtools.wallet.Wallet;
import com.sun.jna.Callback;
import com.sun.jna.Pointer;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;


public class VDR extends IndyJava.API implements AutoCloseable {

    private final Pointer pointer;

    protected VDR(Pointer pointer) {
        this.pointer = pointer;
    }

    public Pointer getPointer() {
        return pointer;
    }

    private static Callback stringCb = new Callback() {

        @SuppressWarnings({"unused", "unchecked"})
        public void callback(int xcommand_handle, int err, String str) {
            CompletableFuture<String> future = (CompletableFuture<String>) removeFuture(xcommand_handle);
            if (!checkResult(future, err)) return;

            String result = str;
            future.complete(result);
        }
    };

    private static Callback voidCb = new Callback() {

        @SuppressWarnings({"unused", "unchecked"})
        public void callback(int xcommand_handle, int err) {

            CompletableFuture<Void> future = (CompletableFuture<Void>) removeFuture(xcommand_handle);
            if (!checkResult(future, err)) return;

            Void result = null;
            future.complete(result);
        }
    };

    private static Callback prepareTxnCb = new Callback() {

        @SuppressWarnings({"unused", "unchecked"})
        public void callback(int xcommand_handle, int err, String namespace, Pointer txn_bytes_raw, int txn_bytes_len, String signature_spec, Pointer bytes_to_sign_raw, int bytes_to_sign_len, String endorsement_spec) {

            CompletableFuture<VdrResults.PreparedTxnResult> future = (CompletableFuture<VdrResults.PreparedTxnResult>) removeFuture(xcommand_handle);
            if (!checkResult(future, err)) return;

            byte[] txn_bytes = new byte[txn_bytes_len];
            txn_bytes_raw.read(0, txn_bytes, 0, txn_bytes_len);

            byte[] bytes_to_sign = new byte[bytes_to_sign_len];
            bytes_to_sign_raw.read(0, bytes_to_sign, 0, bytes_to_sign_len);

            VdrResults.PreparedTxnResult result = new VdrResults.PreparedTxnResult(namespace, txn_bytes, signature_spec, bytes_to_sign, endorsement_spec);
            future.complete(result);
        }
    };

    private static Callback pingCb = new Callback() {

        @SuppressWarnings({"unused", "unchecked"})
        public void callback(int xcommand_handle, int err, String str) {
            CompletableFuture<Map<String, VdrResults.PingResult>> future =
                    (CompletableFuture<Map<String, VdrResults.PingResult>>) removeFuture(xcommand_handle);
            if (!checkResult(future, err)) return;
            JSONObject json = new JSONObject(str);
            HashMap<String, VdrResults.PingResult> result = new HashMap<>();
            for (String key : json.keySet()) {
                JSONObject entry = json.getJSONObject(key);
                String code = entry.getString("code");
                String message = entry.getString("message");
                result.put(key, new VdrResults.PingResult(code, message));
            }

            future.complete(result);
        }
    };

    public CompletableFuture<Map<String, VdrResults.PingResult>> ping(
            List<String> namespaceList
    ) {
        CompletableFuture<Map<String, VdrResults.PingResult>> future = new CompletableFuture<>();
        try {
            ParamGuard.notNull(namespaceList, "namespaceList");
        } catch (Exception ex) {
            future.completeExceptionally(ex);
            return future;
        }

        int commandHandle = addFuture(future);

        Pointer pointer = this.getPointer();

        String namespaces = new JSONArray(namespaceList).toString();

        int result = LibIndy.api.vdr_ping(
                commandHandle,
                pointer,
                namespaces,
                stringCb);

        checkResult(future, result);

        return future;
    }

    public CompletableFuture<Void> cleanup() {
        CompletableFuture<Void> future = new CompletableFuture<>();
        int commandHandle = addFuture(future);

        Pointer pointer = this.getPointer();

        int result = LibIndy.api.vdr_cleanup(
                commandHandle,
                pointer,
                voidCb);

        checkResult(future, result);

        return future;
    }

    public CompletableFuture<String> resolveDID(
            String fqDID
    ) {
        CompletableFuture<String> future = new CompletableFuture<>();

        try {
            ParamGuard.notNull(fqDID, "fqDID");
        } catch (Exception ex) {
            future.completeExceptionally(ex);
            return future;
        }

        int commandHandle = addFuture(future);

        Pointer pointer = this.getPointer();

        int result = LibIndy.api.vdr_resolve_did(
                commandHandle,
                pointer,
                fqDID,
                stringCb);

        checkResult(future, result);

        return future;
    }

    public CompletableFuture<String> resolveDID(
            String fqDID,
            VdrParams.CacheOptions cacheOptions
    ) {
        CompletableFuture<String> future = new CompletableFuture<>();

        try {
            ParamGuard.notNull(fqDID, "fqDID");
            ParamGuard.notNull(cacheOptions, "cacheOptions");
        } catch (Exception ex) {
            future.completeExceptionally(ex);
            return future;
        }

        int commandHandle = addFuture(future);

        Pointer pointer = this.getPointer();

        String cacheOpts = new JSONObject(cacheOptions).toString();

        int result = LibIndy.api.vdr_resolve_did_with_cache(
                commandHandle,
                pointer,
                fqDID,
                cacheOpts,
                stringCb);

        checkResult(future, result);

        return future;
    }

    public CompletableFuture<String> resolveSchema(
            String fqSchema
    ) {
        CompletableFuture<String> future = new CompletableFuture<>();

        try {
            ParamGuard.notNull(fqSchema, "fqSchema");
        } catch (Exception ex) {
            future.completeExceptionally(ex);
            return future;
        }

        int commandHandle = addFuture(future);

        Pointer pointer = this.getPointer();

        int result = LibIndy.api.vdr_resolve_schema(
                commandHandle,
                pointer,
                fqSchema,
                stringCb);

        checkResult(future, result);

        return future;
    }

    public CompletableFuture<String> resolveSchema(
            String fqSchema,
            VdrParams.CacheOptions cacheOptions
    ) {
        CompletableFuture<String> future = new CompletableFuture<>();

        try {
            ParamGuard.notNull(fqSchema, "fqSchema");
            ParamGuard.notNull(cacheOptions, "cacheOptions");
        } catch (Exception ex) {
            future.completeExceptionally(ex);
            return future;
        }

        int commandHandle = addFuture(future);

        Pointer pointer = this.getPointer();

        String cacheOpts = new JSONObject(cacheOptions).toString();

        int result = LibIndy.api.vdr_resolve_schema_with_cache(
                commandHandle,
                pointer,
                fqSchema,
                cacheOpts,
                stringCb);

        checkResult(future, result);

        return future;
    }

    public CompletableFuture<String> resolveCredDef(
            String fqCredDef
    ) {
        CompletableFuture<String> future = new CompletableFuture<>();

        try {
            ParamGuard.notNull(fqCredDef, "fqCredDef");
        } catch (Exception ex) {
            future.completeExceptionally(ex);
            return future;
        }

        int commandHandle = addFuture(future);

        Pointer pointer = this.getPointer();

        int result = LibIndy.api.vdr_resolve_cred_def(
                commandHandle,
                pointer,
                fqCredDef,
                stringCb);

        checkResult(future, result);

        return future;
    }

    public CompletableFuture<String> resolveCredDef(
            String fqCredDef,
            VdrParams.CacheOptions cacheOptions
    ) {
        CompletableFuture<String> future = new CompletableFuture<>();

        try {
            ParamGuard.notNull(fqCredDef, "fqCredDef");
            ParamGuard.notNull(cacheOptions, "cacheOptions");
        } catch (Exception ex) {
            future.completeExceptionally(ex);
            return future;
        }

        int commandHandle = addFuture(future);

        Pointer pointer = this.getPointer();

        String cacheOpts = new JSONObject(cacheOptions).toString();


        int result = LibIndy.api.vdr_resolve_cred_def_with_cache(
                commandHandle,
                pointer,
                fqCredDef,
                cacheOpts,
                stringCb);

        checkResult(future, result);

        return future;
    }

    public CompletableFuture<VdrResults.PreparedTxnResult> prepareDID(
            String txnSpecificParams,
            String submitterDID,
            String endorser
    ) {
        CompletableFuture<VdrResults.PreparedTxnResult> future = new CompletableFuture<>();

        try {
            ParamGuard.notNull(txnSpecificParams, "txnSpecificParams");
            ParamGuard.notNull(submitterDID, "submitterDID");
        } catch (Exception ex) {
            future.completeExceptionally(ex);
            return future;
        }

        int commandHandle = addFuture(future);

        Pointer pointer = this.getPointer();

        int result = LibIndy.api.vdr_prepare_did(
                commandHandle,
                pointer,
                txnSpecificParams,
                submitterDID,
                endorser,
                prepareTxnCb);

        checkResult(future, result);

        return future;
    }

    public CompletableFuture<VdrResults.PreparedTxnResult> prepareSchema(
            String txnSpecificParams,
            String submitterDID,
            String endorser
    ) {
        CompletableFuture<VdrResults.PreparedTxnResult> future = new CompletableFuture<>();

        try {
            ParamGuard.notNull(txnSpecificParams, "txnSpecificParams");
            ParamGuard.notNull(submitterDID, "submitterDID");
        } catch (Exception ex) {
            future.completeExceptionally(ex);
            return future;
        }

        int commandHandle = addFuture(future);

        Pointer pointer = this.getPointer();

        int result = LibIndy.api.vdr_prepare_schema(
                commandHandle,
                pointer,
                txnSpecificParams,
                submitterDID,
                endorser,
                prepareTxnCb);

        checkResult(future, result);

        return future;
    }

    public CompletableFuture<VdrResults.PreparedTxnResult> prepareCredDef(
            String txnSpecificParams,
            String submitterDID,
            String endorser
    ) {
        CompletableFuture<VdrResults.PreparedTxnResult> future = new CompletableFuture<>();

        try {
            ParamGuard.notNull(txnSpecificParams, "txnSpecificParams");
            ParamGuard.notNull(submitterDID, "submitterDID");
        } catch (Exception ex) {
            future.completeExceptionally(ex);
            return future;
        }

        int commandHandle = addFuture(future);

        Pointer pointer = this.getPointer();

        int result = LibIndy.api.vdr_prepare_cred_def(
                commandHandle,
                pointer,
                txnSpecificParams,
                submitterDID,
                endorser,
                prepareTxnCb);

        checkResult(future, result);

        return future;
    }

    public CompletableFuture<String> submitTxn(
            String namespace,
            byte[] txnBytes,
            String signatureSpec,
            byte[] signature,
            String endorsement
    ) {
        CompletableFuture<String> future = new CompletableFuture<>();

        try {
            ParamGuard.notNull(namespace, "namespace");
            ParamGuard.notNull(signatureSpec, "signatureSpec");
            ParamGuard.notNull(txnBytes, "txnBytes");
            ParamGuard.notNull(signature, "signature");
            ParamGuard.notNull(endorsement, "endorsement");
        } catch (Exception ex) {
            future.completeExceptionally(ex);
            return future;
        }

        int commandHandle = addFuture(future);

        Pointer pointer = this.getPointer();

        int result = LibIndy.api.vdr_submit_txn(
                commandHandle,
                pointer,
                namespace,
                txnBytes,
                txnBytes.length,
                signatureSpec,
                signature,
                signature.length,
                endorsement,
                stringCb);

        checkResult(future, result);

        return future;
    }

    public CompletableFuture<String> submitRawTxn(
            String namespace,
            byte[] txnBytes
    ) {
        CompletableFuture<String> future = new CompletableFuture<>();

        try {
            ParamGuard.notNull(namespace, "namespace");
            ParamGuard.notNull(txnBytes, "txnBytes");
        } catch (Exception ex) {
            future.completeExceptionally(ex);
            return future;
        }

        int commandHandle = addFuture(future);

        Pointer pointer = this.getPointer();

        int result = LibIndy.api.vdr_submit_raw_txn(
                commandHandle,
                pointer,
                namespace,
                txnBytes,
                txnBytes.length,
                stringCb);

        checkResult(future, result);

        return future;
    }

    public CompletableFuture<String> submitQuery(
            String namespace,
            String query
    ) {
        CompletableFuture<String> future = new CompletableFuture<>();

        try {
            ParamGuard.notNull(namespace, "namespace");
            ParamGuard.notNull(query, "query");
        } catch (Exception ex) {
            future.completeExceptionally(ex);
            return future;
        }

        int commandHandle = addFuture(future);

        Pointer pointer = this.getPointer();

        int result = LibIndy.api.vdr_submit_query(
                commandHandle,
                pointer,
                namespace,
                query,
                stringCb);

        checkResult(future, result);

        return future;
    }

    public static CompletableFuture<String> indyEndorse(
            Wallet wallet,
            String endorsementData,
            String signatureSpec,
            byte[] txnBytes
    ) {
        CompletableFuture<String> future = new CompletableFuture<>();

        try {
            ParamGuard.notNull(wallet, "wallet");
            ParamGuard.notNull(endorsementData, "endorsementData");
            ParamGuard.notNull(signatureSpec, "signatureSpec");
            ParamGuard.notNull(txnBytes, "txnBytes");
        } catch (Exception ex) {
            future.completeExceptionally(ex);
            return future;
        }

        int commandHandle = addFuture(future);

        int handle = wallet.getWalletHandle();

        int result = LibIndy.api.vdr_indy_endorse(
                commandHandle,
                handle,
                endorsementData,
                signatureSpec,
                txnBytes,
                txnBytes.length,
                stringCb);

        checkResult(future, result);

        return future;
    }

    public CompletableFuture<String> prepareCheqdEndorsementData(
            Wallet wallet,
            String namespace,
            String keyAlias,
            byte[] txnBytes,
            int gas_price,
            String memo
    ) {
        CompletableFuture<String> future = new CompletableFuture<>();

        try {
            ParamGuard.notNull(wallet, "wallet");
            ParamGuard.notNull(namespace, "namespace");
            ParamGuard.notNull(keyAlias, "keyAlias");
            ParamGuard.notNull(memo, "memo");
        } catch (Exception ex) {
            future.completeExceptionally(ex);
            return future;
        }

        int commandHandle = addFuture(future);

        int handle = wallet.getWalletHandle();

        Pointer pointer = this.getPointer();

        int result = LibIndy.api.vdr_prepare_cheqd_endorsement_data(
                commandHandle,
                pointer,
                handle,
                namespace,
                keyAlias,
                txnBytes,
                txnBytes.length,
                gas_price,
                memo,
                stringCb);

        checkResult(future, result);

        return future;
    }

    public static CompletableFuture<String> cheqdEndorse(
            Wallet wallet,
            String endorsementData,
            String signatureSpec,
            byte[] txnBytes,
            byte[] signature
    ) {
        CompletableFuture<String> future = new CompletableFuture<>();

        try {
            ParamGuard.notNull(wallet, "wallet");
            ParamGuard.notNull(endorsementData, "endorsementData");
            ParamGuard.notNull(signatureSpec, "signatureSpec");
            ParamGuard.notNull(txnBytes, "txnBytes");
            ParamGuard.notNull(signature, "signature");
        } catch (Exception ex) {
            future.completeExceptionally(ex);
            return future;
        }

        int commandHandle = addFuture(future);

        int handle = wallet.getWalletHandle();

        int result = LibIndy.api.vdr_cheqd_endorse(
                commandHandle,
                handle,
                endorsementData,
                signatureSpec,
                txnBytes,
                txnBytes.length,
                signature,
                signature.length,
                stringCb);

        checkResult(future, result);

        return future;
    }

    @Override
    public void close() throws InterruptedException, ExecutionException, IndyException {
        cleanup().get();
    }
}
