package com.evernym.vdrtools.vdr;

import com.evernym.vdrtools.IndyException;
import com.evernym.vdrtools.IndyIntegrationTestWithSingleWallet;
import com.evernym.vdrtools.did.Did;
import com.evernym.vdrtools.did.DidResults;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.evernym.vdrtools.utils.PoolUtils.buildGenesisTransactionsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class VdrTest extends IndyIntegrationTestWithSingleWallet {

    @Test
    public void testVdrOpenIdnyAndCheqdPools() throws Exception, IndyException {
        VdrBuilder builder = VdrBuilder.create();
        assertNotNull(builder);

        builder.registerIndyLedger(INDY_NAMESPACES_LIST, buildGenesisTransactionsString(), null).get();
        builder.registerCheqdLedger(CHEQD_NAMESPACES_LIST, CHEQD_CHAIN_ID, CHEQD_POOL_IP).get();
        VDR vdr = builder.build();
        assertNotNull(vdr);

        JSONObject didJson = new JSONObject();
        didJson.put("seed", TRUSTEE_SEED);
        didJson.put("method_name", INDY_NAMESPACE);
        DidResults.CreateAndStoreMyDidResult result = Did.createAndStoreMyDid(wallet, didJson.toString()).get();

        List<String> namespacesList = Arrays.asList(INDY_NAMESPACE, CHEQD_NAMESPACE);

        vdr.ping(namespacesList).get();

        ArrayList<String> didDocs = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            String res = vdr.resolveDID(result.getDid()).get();
            didDocs.add(res);
        }

        JSONObject didDoc = new JSONObject(didDocs.get(0));

        assertEquals(DID_TRUSTEE, didDoc.getString("did"));

        vdr.cleanup().get();
    }
}
